import React from "react";

/**
 * These are added to mimic StyledComponents without the dependency
 */

const Avatar = ({ avatar, alt }) => <img src={avatar} alt={alt} />;
const Message = ({ message }) => <h3>{message}</h3>;
const FullName = ({ fullName }) => <p>{fullName}</p>;
const TimeStamp = ({ timestamp }) => <p> {timestamp}</p>;

export { Avatar, Message, FullName, TimeStamp };
