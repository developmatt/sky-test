import React from "react";
import renderer from "react-test-renderer";
import { Avatar, FullName, Message, TimeStamp } from "./Information";

const mockObj = {
  message: "Quisque erat eros, viverra eget, congue eget, semper rutrum, null",
  avatar: "http://dummyimage.com/100x100.jpg/5fa2dd/ffffff",
  fullName: "Albert Rose",
  timestamp: "Thu Feb 09 2017"
};

describe("<Avatar />", () => {
  it("renders correctly", () => {
    const tree = renderer
      .create(<Avatar avatar={mockObj.avatar} alt={"alternative"} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe("<FullName", () => {
  it("renders correctly", () => {
    const tree = renderer
      .create(<FullName fullName={mockObj.fullName} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe("<Message", () => {
  it("renders correctly", () => {
    const tree = renderer
      .create(<Message message={mockObj.message} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});

describe("<Timestamp />", () => {
  it("renders correctly", () => {
    const tree = renderer
      .create(<TimeStamp timestamp={mockObj.timestamp} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
