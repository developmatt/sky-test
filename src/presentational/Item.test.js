import React from "react";
import ViewContainer from "./ViewContainer";
import Items from "./Items";
import {
  mockedArrayLengthOf4,
  mockedArrayLengthOf2
} from "../helpers/fixtures";

describe("<ViewContainer />", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<ViewContainer items={[]} />);
  });
  it("should render without throwing an error", () => {
    expect(wrapper.contains(<div className="avatar-container" />)).toBe(true);
  });

  it('should be selectable by class "avatar-container"', () => {
    expect(wrapper.is(".avatar-container")).toBe(true);
  });
});

describe("state", () => {
  it("should be an empty array intitally", () => {
    const wrapper = shallow(<ViewContainer items={[]} />);
    expect(wrapper.props("items").children).toEqual([]);
    expect(wrapper.props("items").children).toEqual(expect.arrayContaining([]));
  });

  it("should be an array of objects", () => {
    const wrapper = shallow(
      <ViewContainer items={mockedArrayLengthOf4.items} />
    );
    const wrapperInstance = wrapper.instance();
    expect(wrapperInstance.props.items).toEqual(mockedArrayLengthOf4.items);
    expect(wrapperInstance.props.items).toEqual(
      expect.arrayContaining(mockedArrayLengthOf4.items)
    );
  });
});

describe("ui", () => {
  it("should render the <Items /> component", () => {
    const wrapper = mount(<ViewContainer items={mockedArrayLengthOf4.items} />);
    expect(wrapper.find(<Items />)).toBeTruthy();
  });

  it("renders an array of 4 <Items />", () => {
    const wrapper = mount(<ViewContainer items={mockedArrayLengthOf4.items} />);
    const itemsLength = mockedArrayLengthOf4.items.length;

    expect(wrapper.find(<Items />)).toBeDefined();
    expect(wrapper.find("Items")).toHaveLength(itemsLength);
  });

  it("renders an array of 2 <Items />", () => {
    const wrapper = mount(<ViewContainer items={mockedArrayLengthOf2.items} />);
    const itemsLength = mockedArrayLengthOf2.items.length;

    expect(wrapper.find(<Items />)).toBeDefined();
    expect(wrapper.find("Items")).toHaveLength(itemsLength);
  });

  it("should render the correct props for index 0", () => {
    const wrapper = mount(<ViewContainer items={mockedArrayLengthOf4.items} />);
    expect(wrapper.find("Items").get(0).props).toEqual({
      avatar: "http://dummyimage.com/100x100.jpg/5fa2dd/ffffff",
      email: "arosec@bbb.org",
      fullName: "Albert Rose",
      message:
        "Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.",
      messageId: "cd445e6d-e514-424f-ba8f-16ec842002c6",
      timestamp: "Thu Feb 09 2017",
      userId: "fe27b760-a915-475c-80bb-7cdf14cc6ef3"
    });
  });
});
