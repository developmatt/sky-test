import React from "react";
import { Avatar, FullName, Message, TimeStamp } from "./Information";
import Email from "./Email";

import "./Items.css";

export default function Items({ message, timestamp, email, fullName, avatar }) {
  return (
    <div className="item">
      <div className="item__content">
        <Avatar avatar={avatar} alt={"Avatar"} />
        <Message message={message} />
        <FullName fullName={fullName} />
        <Email email={email} />
        <TimeStamp timestamp={timestamp} />
      </div>
    </div>
  );
}
