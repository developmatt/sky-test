import React from "react";
import Email from "./Email";
import renderer from "react-test-renderer";

describe("<Email />", () => {
  const emailText = "phoward12@chronoengine.com";

  it("renders correctly", () => {
    const tree = renderer.create(<Email email={emailText} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("changes the class when hovered", () => {
    const component = renderer.create(<Email email={emailText} />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    tree.props.onMouseEnter();

    tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    tree.props.onMouseLeave();
    tree = component.toJSON();

    expect(tree).toMatchSnapshot();
    expect(tree.props.className).toEqual("normal");
  });

  it("should render href", () => {
    const component = renderer.create(<Email email={emailText} />);
    let tree = component.toJSON();
    expect(tree.props.href).toEqual("mailto: phoward12@chronoengine.com");
  });
});
