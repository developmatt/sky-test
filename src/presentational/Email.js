import React, { Component } from "react";
import "./Email.css";

const STATUS = {
  HOVERED: "hovered",
  NORMAL: "normal"
};

export default class Email extends Component {
  state = {
    class: STATUS.NORMAL
  };

  clickHandlers = {};

  getClickHandler(key) {
    if (!Object.prototype.hasOwnProperty.call(this.clickHandlers, key)) {
      this.clickHandlers[key] = () => this.setState({ class: key });
    }
    return this.clickHandlers[key];
  }

  render() {
    const { email } = this.props;
    return (
      <a
        href={`mailto: ${email}`}
        className={this.state.class}
        onMouseEnter={this.getClickHandler(STATUS.HOVERED)}
        onMouseLeave={this.getClickHandler(STATUS.NORMAL)}
      >
        {email}
      </a>
    );
  }
}
