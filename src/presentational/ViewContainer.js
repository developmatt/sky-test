import React from "react";
import Items from "./Items";
import PropTypes from "prop-types";
import "./ViewContainer.css";

export default function ViewContainer({ items }) {
  const createitems = articles => {
    return articles.map(article => (
      <Items key={article.messageId} {...article} />
    ));
  };

  return (
    <div className={"avatar-container"}>{items && createitems(items)}</div>
  );
}

ViewContainer.propTypes = {
  items: PropTypes.array
};
