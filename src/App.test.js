import React from 'react';
import renderer from 'react-test-renderer';
import App from './App';
import { mockedArrayLengthOf4 } from './helpers/fixtures'

it('renders without crashing', () => {
  const tree = renderer.create(<App />).toJSON();
    expect(tree).toMatchSnapshot();
});

describe('state', () => {
  const initialState = { "root": {"items": [] } };
  
  it("should be an empty array intitally", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.state()).toEqual(initialState)
  });

  it("should be an array of objects", () => {
    const wrapper = shallow(<App />);
    wrapper.setState({ root: { items: mockedArrayLengthOf4.items } });
    expect(wrapper.state()).toEqual({ root: { items: mockedArrayLengthOf4.items } })
  })
})
