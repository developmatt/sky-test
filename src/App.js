import React, { Component } from "react";
import { getMessages, getMembers } from "./data";
import { filterIds, formatData } from "./helpers/utils";
import ViewContainer from "./presentational/ViewContainer";
import "./App.css";

export default class App extends Component {
  state = {
    root: {
      items: []
    }
  };

  componentDidMount() {
    this.fetchAndFormatData(getMessages(), getMembers());
  }

  async fetchAndFormatData(messagesPromise, membersPromise) {
    try {
      const requestData = await Promise.all([messagesPromise, membersPromise]);
      const [messages, members] = [requestData[0], requestData[1]];
      const items = messages.map(msg => {
        const formatted = formatData(filterIds(members, msg)[0], msg);
        delete msg.id;
        return { ...msg, ...formatted };
      });

      this.setState({
        root: {
          items
        }
      });
    } catch (error) {
      throw new Error(`Something went wrong, ${error}`);
    }
  }

  render() {
    const {
      root: { items }
    } = this.state;
    return (
      <div>
        {items ? <ViewContainer items={items} /> : <p>No items available</p>}
      </div>
    );
  }
}
