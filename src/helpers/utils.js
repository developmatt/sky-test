function filterIds(members, messages) {
  return members.filter(member => member.id === messages.userId);
}

const formatDate = timestamp =>  new Date(timestamp).toDateString();

function formatData(members, messages) {
  const { firstName, lastName, avatar, email } = members;
  const { userId, timestamp, message, id } = messages;
  return {
    messageId: id,
    userId,
    fullName: `${firstName} ${lastName}`,
    timestamp: formatDate(timestamp),
    email,
    message,
    avatar
  };
}

export { filterIds, formatData };
