export const mockedArrayLengthOf4 = {
    items: [
      {
        userId: "fe27b760-a915-475c-80bb-7cdf14cc6ef3",
        message:
          "Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.",
        timestamp: "Thu Feb 09 2017",
        messageId: "cd445e6d-e514-424f-ba8f-16ec842002c6",
        fullName: "Albert Rose",
        email: "arosec@bbb.org",
        avatar: "http://dummyimage.com/100x100.jpg/5fa2dd/ffffff"
      },
      {
        userId: "16373df5-da0a-4074-8295-f916b94269f4",
        message:
          "Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.",
        timestamp: "Wed Nov 09 2016",
        messageId: "b03569ae-ccbf-4975-8040-4daba638b407",
        fullName: "Larry Owens",
        email: "lowensm@earthlink.net",
        avatar: "http://dummyimage.com/100x100.bmp/5fa2dd/ffffff"
      },
      {
        userId: "976c4919-a8b4-4807-bebb-84ca8448be32",
        message: "Suspendisse potenti.",
        timestamp: "Fri Jun 03 2016",
        messageId: "3a59859e-33f1-4c2b-a636-1f119c484dc8",
        fullName: "Peter Howard",
        email: "phoward12@chronoengine.com",
        avatar: "http://dummyimage.com/100x100.jpg/5fa2dd/ffffff"
      },
      {
        userId: "d18b107b-6874-49bd-94c0-4d830fc7eaed",
        message: "Nunc purus.",
        timestamp: "Thu Apr 07 2016",
        messageId: "c7252640-af98-442a-9dcd-ddc5e789238a",
        fullName: "Marilyn Andrews",
        email: "mandrews4@google.it",
        avatar: "http://dummyimage.com/100x100.png/ff4444/ffffff"
      }
    ]
};

export const mockedArrayLengthOf2 = {
  items: [
    {
      userId: "16373df5-da0a-4074-8295-f916b94269f4",
      message:
        "Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.",
      timestamp: "Wed Nov 09 2016",
      messageId: "b03569ae-ccbf-4975-8040-4daba638b407",
      fullName: "Larry Owens",
      email: "lowensm@earthlink.net",
      avatar: "http://dummyimage.com/100x100.bmp/5fa2dd/ffffff"
    },
    {
      userId: "d18b107b-6874-49bd-94c0-4d830fc7eaed",
      message: "Nunc purus.",
      timestamp: "Thu Apr 07 2016",
      messageId: "c7252640-af98-442a-9dcd-ddc5e789238a",
      fullName: "Marilyn Andrews",
      email: "mandrews4@google.it",
      avatar: "http://dummyimage.com/100x100.png/ff4444/ffffff"
    }
  ]
};
